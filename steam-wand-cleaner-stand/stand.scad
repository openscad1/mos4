
include <../libopenscad/mcube.scad>
include <../libopenscad/mtube.scad>
include <../libopenscad/mcylinder.scad>

inch = 25.4;


jug_diameter = 91;

module jug_slug() {

    color("orange")
    mcylinder(h = 100, d = jug_diameter, align = [0, 0, 1], chamfer = 2, color = "orange");
    
}

//jug_slug();

leg_width = 10;
leg_depth = 51;
leg_height = 50;
legs = 5;

chamfer = 2;

skinny = 1;

for(z = [1:legs]) {
    rotate([0, 0, z * 360 / legs]) {

        color("pink")
        translate([-leg_width/2, 0, 0]) {
            mcube([leg_depth + leg_width, leg_width * skinny, leg_height], chamfer = chamfer, align = [1, 0, -1]);
        }
  
        translate([leg_depth, 0, -leg_width]) {

            color("green")
            mcube([leg_width, leg_width * skinny, leg_width * 2], align = [0, 0, 1], chamfer = chamfer);

        }
    }
}



