#!/bin/bash

for x in $@ ; do
    echo make openscad.artifacts/$x | sed 's/[.]scad$/.stl/'
done

#EOF
