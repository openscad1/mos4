include <story-stick-common.scad>;


story_width = 0.66 * inch;
story_length = 5 * inch;


debug = false;

story_stick(story_width = story_width, story_length = story_length, smileys = 3, marker_offset = 6);
