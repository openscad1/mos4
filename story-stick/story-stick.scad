include <story-stick-common.scad>;

debug = false;

story_stick(
    story_length = 7 * inch, 
    smileys = 3, 
    marker_offset = 10, 
    rule_guides = true, 
    cleat_width = 3, 
    cleat_thickness = 2.5
);
