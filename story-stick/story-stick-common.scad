// NOSTL

include <../libopenscad/mcube.scad>
include <../libopenscad/mtube.scad>
include <../libopenscad/mcylinder.scad>

inch = 25.4;

default_story_width = 1 * inch;
default_story_length = 6 * inch;
default_story_thickness = 1/8 * inch;
default_story_marker_ratio = 0.75;
default_story_chamfer_ratio = 0.125;
default_cleat_thickness = 3.5;
default_smileys = 1; 
default_marker_offset = 0;
default_cleat_width = 5;
default_rule_guides = true;

debug = true;


$fn = !is_undef(debug) && debug ? 18 : 180;




module story_stick(story_width = default_story_width, 
    story_length = default_story_length,
    story_thickness = default_story_thickness,
    story_marker_ratio = default_story_marker_ratio, 
    story_chamfer_ratio = default_story_chamfer_ratio,
    cleat_thickness = default_cleat_thickness,
    smileys = default_smileys,
    marker_offset = default_marker_offset,
    cleat_width = default_cleat_width,
    rule_guides = default_rule_guides) 
{

story_chamfer = story_thickness * story_chamfer_ratio;

story_marker = story_width * default_story_marker_ratio; 

    difference() {
        union() {
        // t-bar
        mcube([story_length, story_width, story_thickness], align = [1, 0, 0], chamfer = story_chamfer);
            translate([0, 0, -story_thickness / 2]) {
                mcube([story_width / 2, story_width * cleat_width, story_thickness * cleat_thickness], align=[1, 0, 1], chamfer = story_chamfer);
            }
        }



        union() {
            translate([marker_offset, 0, 0]) {

                // smiley(s) :)
                for(x = [0 : 1 : smileys - 1]) { 
                    translate([(story_width * x) + (story_width / 2 + (story_width - story_thickness)/2), 0, 0]) {
                        mcylinder(d = story_marker - story_chamfer * 2, h = story_thickness * 3, align = [0, 0, 0], chamfer = story_chamfer);

                        color("red") translate([0, 0, story_thickness/2 - story_chamfer]) {
                            mcylinder(d = story_marker, h = story_thickness * 3, align = [0, 0, 1], chamfer = story_chamfer * 1);
                        }
                        color("pink") translate([0, 0, -(story_thickness/2 - story_chamfer)]) {
                            mcylinder(d = story_marker, h = story_thickness * 3, align = [0, 0, -1], chamfer = story_chamfer * 1);
                        }
                    }
                }
                
                
                if(rule_guides) {
                    // rule guides
                    translate([ -story_width / 2, 0 * story_width / 2, 0]){
                        rule_marker = 4;
                        rule_chamfer = rule_marker / 8;
                        for(x = [marker_offset > ((1 - story_marker_ratio) * story_width) ? 0 : 1 : 1 : smileys]) { 
                            translate([(story_width * x) + (story_width / 2 + (story_width - story_thickness)/2), 0, 0]) {
                                mcylinder(d = rule_marker - rule_chamfer * 2, h = story_thickness * 3, align = [0, 0, 0], chamfer = rule_chamfer);

                                color("red") translate([0, 0, story_thickness/2 - rule_chamfer]) {
                                    mcylinder(d = rule_marker, h = story_thickness * 3, align = [0, 0, 1], chamfer = rule_chamfer * 1);
                                }
                                color("pink") translate([0, 0, -(story_thickness/2 - rule_chamfer)]) {
                                    mcylinder(d = rule_marker, h = story_thickness * 3, align = [0, 0, -1], chamfer = rule_chamfer * 1);
                                }
                            }
                        }
                    }
                }






            }
        }
    }
}

story_width = 0.66 * inch;
story_length = 4 * inch;




if(!is_undef(debug) && debug) {
    color("lightgreen") {
        story_stick(rule_guides = false);
    }
    color("lightblue") translate([0, 100, 0])
    story_stick(story_width = story_width, story_length = story_length, smileys = 3, marker_offset = 5);

}
