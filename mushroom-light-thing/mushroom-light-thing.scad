// NOSTL

include <../libopenscad/mcube.scad>
include <../libopenscad/mtube.scad>
include <../libopenscad/mcylinder.scad>

inch = 25.4;

$fn = 180;

module slug() {
    color("grey") {
        mcylinder(d = 30, h = 20, align = [0, 0, 1]);
        
    }
    color("pink") {
 //       mcylinder(d = mushroom_base_id, h = 2, align = [0, 0, -1]);
    }
}




light_width = 30;
light_height = 20;

mushroom_cap_id = 22;
mushroom_base_id = 58;
mushroom_base_height = 50;

mushroom_interior_height = 76;

light_offset_from_base = 10;

leg_width = inch / 16;
leg_chamfer = leg_width / 4;





legs = 5;

module top() {
    difference() {
        union() {
            for(z = [1:legs]) {

                rotate([0, 0, z * 360 / legs]) {


    //            union() {
                color("blue") hull() {
                        translate([0, 0, mushroom_interior_height]) {
                            color("lightblue") {
                                mcube([(mushroom_cap_id / 2), leg_width, leg_width ], chamfer = leg_chamfer, align = [1, 0, -1] );
                            }
                        }
                        translate([0, 0, mushroom_base_height]) {
                            color("cyan") {
                                mcube([(mushroom_base_id / 2), leg_width, leg_width ], chamfer = leg_chamfer, align = [1, 0, -1] );
                            }
                        }
                    }
//                    union() {
                    color("red") hull() {
                        translate([0, 0, mushroom_base_height]) {
                            color("grey") {
                                mcube([(mushroom_base_id / 2), leg_width, leg_width ], chamfer = leg_chamfer, align = [1, 0, -1] );
                            }
                        }
                        translate([0, 0, light_height]) {
                            color("cyan") {
                                mcube([(mushroom_base_id / 2), leg_width, leg_width ], chamfer = leg_chamfer, align = [1, 0, -1] );
                            }
                        }
                    }
                
                        
                    squeeze = 1;
                        
                        translate([(light_width - squeeze)/ 2, 0, 0]) {
                            
                            color("fuchsia") {

                                translate([(mushroom_base_id - (light_width - squeeze)) / 4, 0, light_offset_from_base]) {

//                                    union() {
                                    hull() {

                                        mcube([(mushroom_base_id - (light_width - squeeze)) / 3, leg_width, leg_width], chamfer = leg_chamfer, align = [0, 0, 1] );
                                        mcube([(mushroom_base_id - (light_width - squeeze)) / 6, leg_width, leg_width], chamfer = leg_chamfer, align = [-2, 0, 3] );

                                        translate([0, 0, light_height/2]) {
                                            mcube([(mushroom_base_id - (light_width - squeeze)) / 2, leg_width, leg_width*3], chamfer = leg_chamfer, align = [0, 0, -1] );
                                        }
                                    }
                                }
                            }
                        
                            color("lightblue") {
//                                translate([(mushroom_base_id - light_width) / 4, 0, light_offset_from_base]) {
                                translate([leg_width * 3 / 2, 0, light_offset_from_base]) {
                                    mcylinder(d = leg_width * 3, h = mushroom_base_height * 0.75, chamfer = leg_width * 1, align = [0, 0, 1]);
                                }
                            }

                        }

                }    
            }
                                color("orange") {
                        translate([0, 0, light_height - leg_width]) {
                            mcylinder(d=leg_width * 3, h = (mushroom_interior_height - light_height) + leg_width, chamfer = leg_width * 0.5, align = [0, 0, 1]);
                        }
                    }

            color("green") {
                translate([0, 0, mushroom_interior_height]) {
                    mcylinder(d = mushroom_cap_id, h = leg_width, chamfer = leg_width / 4, align = [0, 0, -1]);
                }
            }

        }


        union() {
    //        slug();
        }

    }
}


module bottom() {
    
    difference() {
        
        union() {
            for(z = [1:legs]) {

                rotate([0, 0, z * 360 / legs]) {

                    translate([light_width/2, 0, 0]) {
                        color("pink") {
                            mcube([(mushroom_base_id - light_width ) / 2, leg_width, light_height * 0.8 ], chamfer = leg_chamfer, align = [1, 0, 1] );
                        }
                        translate([(mushroom_base_id - light_width ) / 4, 0, 0]) {
                            color("lightgreen") {
                                mcylinder(d = leg_width * 3, h = light_height * 0.8, chamfer = leg_width * 1, align = [0, 0, 1]);
                            }
                        }
                    }

                    color("cyan") {
                        mcube([(mushroom_base_id / 2), leg_width, light_height * 0.6 ], chamfer = leg_chamfer, align = [1, 0, 1] );
                    }

                }
            }

            color("orange") {
                mcylinder(d=leg_width * 3, h = light_height * 0.6, chamfer = leg_width * 0.5, align = [0, 0, 1]);
            }

        }


        union() {
//            slug();
        }

    }
}


debug = true;

if(!is_undef(debug) && debug) {
    translate([-mushroom_base_id / 2, 0, 0]) top();
    translate([ mushroom_base_id / 2, 0, 0]) bottom();
}

