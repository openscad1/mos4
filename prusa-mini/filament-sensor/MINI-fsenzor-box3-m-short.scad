use <MINI-fsenzor-box3.scad>


rotate([90, 0, 0]) {
    difference() {
        male();
        mcube([100, 100, 100], align = [1, 0, 0], color = "green");
    }
    
    translate([-35 / 4, 0, 0]) {
        difference() {
            male();
            mcube([100, 100, 100], align = [-1, 0, 0], color = "purple");
        }
    }    
}

