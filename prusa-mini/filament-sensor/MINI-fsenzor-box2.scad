

module clamp() {
    difference(){
    intersection() {
//    union(){
        color("cyan") {
            import("MINI-fsenzor-box.stl");
        }
        translate([55, -64.1, 0]) color("red") cube([100, 100, 100], center = true);
    }
    color("blue") {
        translate([8.1, -13, -8])
        cube([5.5, 5, 10], center = true);
    }
}
}

module clamp_long() {
difference() {
    intersection() {
//    union(){
        color("cyan") {
            import("MINI-fsenzor-box.stl");
        }
        translate([45, -64.1, 0]) color("red") cube([100, 100, 100], center = true);
    }
    color("blue") {
        translate([8.1, -13, -8])
        cube([5.5, 5, 10], center = true);
    }
}
}



union() {
    
    import("MINI-fsenzor-box.stl");

    translate([-28, -0, 0]) {
        rotate([180, 0, 180]) {
            clamp();
        }
    }

    translate([-45, -0, 0]) {
        rotate([180, 0, 180]) {
            clamp_long();
        }
    }


}

