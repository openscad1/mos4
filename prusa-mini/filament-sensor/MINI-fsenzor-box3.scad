include <../../libopenscad/mcube.scad>
include <../../libopenscad/mtube.scad>
include <../../libopenscad/mcylinder.scad>


ptfe_tube_od = 4;
ptfe_anchor_length = 35;
ptfe_anchor_width = 18;
ptfe_anchor_height = 22.5;

m3_screw_cap_head_diameter = 5.6;
m3_screw_cap_head_height = 3;

m3_screw_diameter = 3.25;

m3_hex_nut_face_to_face = 5.5;

m3_hex_nylock_nut_height = 3.25;
m3_hex_nylock_face_width = 3.175;

ziptie_width = 5;

manifold_tollerance = 0.01;

block_gap = 0.1;

module block() { 

    translate([0, block_gap, 0]) {

        translate([-ptfe_anchor_length / 2, 0, 0]) {
            mcube([ptfe_anchor_length, ptfe_anchor_width / 2 - block_gap, ptfe_anchor_height], align = [1, 1, 0], color = "orange", chamfer = 1);
        }

    }

}
  

module block2() { 

    translate([0, block_gap, 0]) {
        translate([0, 0, 0]) {
            mcube([ptfe_anchor_length , ptfe_anchor_width / 2 - block_gap, ptfe_anchor_height - 2], align = [0, 1, 0], color = "green", chamfer = 1);
        }

        translate([-ptfe_anchor_length / 2, 0, 0]) {
            mcube([ptfe_anchor_length * 3/4 - ziptie_width / 2, ptfe_anchor_width / 2 - block_gap, ptfe_anchor_height], align = [1, 1, 0], color = "orange", chamfer = 1);
        }
        translate([ptfe_anchor_length / 2, 0, 0]) {
            mcube([ptfe_anchor_length * 1/4 - ziptie_width / 2, ptfe_anchor_width / 2 - block_gap, ptfe_anchor_height], align = [-1, 1, 0], color = "orange", chamfer = 1);
        }
    }

}
  


module tube_channel() {
    rotate([0, 90, 0]) {
        mcylinder(d = ptfe_tube_od, h = 200, color = "red", $fn = 90, align = [0, 0, 0]);
    }
}


module m3_cap_head_screw_male() {
    rotate([0, 90, -90]) { 
        mcylinder(d = m3_screw_diameter, h = 30, align = [0, 0, 1], $fn = 180);
        translate([0, 0, manifold_tollerance])
        mcylinder(d = m3_screw_cap_head_diameter, h = m3_screw_cap_head_height + manifold_tollerance * 2, align = [0, 0, -1], $fn = 180);
    }
}


module nylock_nut() {
    


color("blue") hull()
    for(a = [0, 120, 240]) {
        echo(a);
        rotate([0, a, 0])
        translate([0, 0, 0])
        mcube([m3_hex_nylock_face_width, m3_hex_nylock_nut_height, m3_hex_nut_face_to_face], center = true, color = "blue");
    }
}

module male() {
    difference() {
        union() {
            // positive
            block();
        }
        union() {
            // negative
            tube_channel();
            for(z = [-1, 1]) {
                translate([0, 0, z * ptfe_anchor_height / 4]) {
                    screws();
                }
            }

        }
    }




}


module female() {
    difference() {
        union() {
            // positive
            rotate([0, 180, 180]) {
                block();
//                block2();
            }
        }
        union() {
            // negative
            tube_channel();
            for(z = [-1, 1]) {
                translate([0, 0, z * ptfe_anchor_height / 4]) {

                    screws();
                    translate([0, -ptfe_anchor_width/2 + (m3_screw_cap_head_height / 2) - manifold_tollerance, 0]) {
                        translate([-ptfe_anchor_length/4, 0, 0]) nylock_nut();
                                                                 nylock_nut();
                        translate([ ptfe_anchor_length/4, 0, 0]) nylock_nut();
                    }
                }

            }

        }
    }
}


    
module female2() {
    difference() {
        female();
        rotate([90, 0, 0]) {
            translate([-ptfe_anchor_length/2 + 3, -ptfe_anchor_width * (2/3), 0])
            mcylinder(d = 7, h = 20, color = "red", $fn = 90);
        }
    }
}

module female3() {
    difference() {
        female();
        union() {
            rotate([90, 0, 0]) {
                translate([ptfe_anchor_length/2 - 4, -ptfe_anchor_width * (2/3), 0]) {
                    mcylinder(d = 5, h = 20, color = "red", $fn = 90);
                }
                translate([16, 0, 0]) {
                    mcube([30, 30, 30], align = [1,-1,0] , color = "blue");
                }
            }
        }
    }
}

module screws() {
    translate([0, ptfe_anchor_width/2 - m3_screw_cap_head_height, 0]) {
        translate([-ptfe_anchor_length/4, 0, 0]) m3_cap_head_screw_male();
                                                 m3_cap_head_screw_male();
        translate([ ptfe_anchor_length/4, 0, 0]) m3_cap_head_screw_male();
    }
}


// debug = true;

if(!is_undef(debug)) {
    male();
    female();
}


import("MINI-fsenzor-box.stl");

translate([29, -20.5, 0]) {
    rotate([90, 0, 0]) {
        female2();
        
    }
}
translate([-48, -20.5, 0]) {
    rotate([90, 0, 0]) {
        female3();
    }
}
