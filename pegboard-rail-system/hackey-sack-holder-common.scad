// NOSTL

include <../libopenscad/mcube.scad>
include <../libopenscad/mtube.scad>
include <../libopenscad/mcylinder.scad>

inch = 25.4;

default_hackey_sack_diameter = 2.5 * inch;


default_wall_thickness = inch / 8;
default_chamfer = default_wall_thickness / 4;


debug = true;

debug_slug = true;
debug_model = true;


colors = ["red", "white", "blue", "grey", "orange", "salmon", "pink", "green"];


$fn = (!is_undef(debug) && !debug) ? 360 : 36;

module hackey_sack_slug(hackey_sack_diameter = default_hackey_sack_diameter) {

    translate([0, 0, hackey_sack_diameter / 2])
    for(x = [-1, 1]) {
        for(y = [-1, 1]) {
            for(z = [-1, 1]) {
               translate([-0.01 * x, -0.01 * y, -0.01 * z])
               color(colors[(x + 1) / 2 + (y + 1) + (z + 1) * 2]) {
                    intersection() {
                        sphere(d = hackey_sack_diameter);
                        mcube([hackey_sack_diameter, hackey_sack_diameter, hackey_sack_diameter], align = [x, y, z]);
                    }
                }
            }
        }
    }


}

  
    


module hackey_sack_holder(hackey_sack_diameter = default_hackey_sack_diameter, wall_thickness = default_wall_thickness, chamfer = default_chamfer) {
    

    color("orange")
    translate([0, 0, wall_thickness])
    difference() {
        mcylinder(h = hackey_sack_diameter / 2, d = hackey_sack_diameter , chamfer = chamfer, align = [0, 0, 1]);
        translate([0, 0, hackey_sack_diameter / 2]) sphere(d = hackey_sack_diameter + 0.01);
    }
    



    color("lightblue") {
       mtube(h = hackey_sack_diameter / 2 + wall_thickness, id = hackey_sack_diameter, od = hackey_sack_diameter + wall_thickness * 2, chamfer = chamfer, align = [0, 0, 1]);
        mcylinder(h = wall_thickness, d = hackey_sack_diameter + wall_thickness * 2, chamfer = chamfer, align = [0, 0, 1]);
    }


}

if(!is_undef(debug) && debug) { 
    
    if(!is_undef(debug_slug) && debug_slug) {
        hackey_sack_slug();
    }  

    if(!is_undef(debug_model) && debug_model) {
        hackey_sack_holder();
    }
}

