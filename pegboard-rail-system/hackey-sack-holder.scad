include <hackey-sack-holder-common.scad>

debug = false;
//debug_me = true;



if(!is_undef(debug_me) && debug_me) {
    intersection() {
        hackey_sack_holder();
        cube([100, 100, 100]);
    }
} else {
    hackey_sack_holder();
}


