// BOF

include <cleat-and-rail-common.scad>

debug = false;
//debug_cleat = false;
//debug_rail = false;


//default_finish_fn = 90;


standoff = 2;
standoff_tenth_mm = 1;
rotate([0,90,0]) // turn on side for printing
difference() {

    union() {
        cleat_model(
            horn_standoff = standoff + (standoff_tenth_mm * 0.1),
            horn_lean_in = 30,
            horn_arc = 45,
            horn_arc_radius = (inch / 4),
            horn_diameter = (inch / 4) - 1
        );
    }
    

    union() {
        translate([-(inch * (3/8)) , -inch * 3/16 + (default_plate_back_margin/2), inch * 1/8]) {


            color("fuchsia")
            for(z = [0 : standoff - 1]){
                translate([0, 0, z * inch / 8])
                rotate([90, 0, 0]) {
                    mcylinder(d = inch / 16, h = inch/8, align = [0, 0, -1.125]);
                }
            }

            
            color("lightblue")
            for(z = [0 : standoff_tenth_mm - 1]){
                translate([0, 0, z * inch / 8])
                rotate([90, 0, 0]) {
                    mcylinder(d = inch / 16, h = inch/8, align = [0, 0, 1.125]);
                }
            }



        }
    }
}
// EOF

/*
default_horn_arc = 75;
default_horn_arc_radius = inch / 5;
default_horn_diameter = inch * 0.225;
default_horn_standoff = 0.5;
default_plate_back_margin = 0.25;
default_plate_width_margin = 0.25;
default_plate_height_margin = 0.25;
default_horn_lean_in = 0;

*/

