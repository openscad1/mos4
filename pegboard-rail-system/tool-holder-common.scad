// NOSTL

include <../libopenscad/mbox2.scad>
include <../libopenscad/mcylinder.scad>
include <../libopenscad/mcube.scad>




debug = true;

//debug_slug = true;
debug_model = true;
debug_mbox = false;

$fn = 12;

module tool_slug() {

  


}


default_ow = 1;
default_ol = 2;
default_oh = 4;

default_or = 5;




module tool_holder(ow = default_ow, ol = default_ol, oh = default_oh, or = default_or, wall_thickness = default_wall_thickness, chamfer = default_chamfer, cutout_ratio = [0.25, 0.75], minkowski = false, label = true) {

    outside_height = inch * oh - (minkowski ? wall_thickness : 0);
    outside_length = inch * ol - (minkowski ? wall_thickness : 0);
    outside_width = inch * ow - (minkowski ? wall_thickness : 0);
    
    module basic_box() {
        translate([0, outside_length / 2 + (minkowski ? wall_thickness / 2 : 0), outside_height / 2 + (minkowski ? wall_thickness / 2 : 0)]) {
            // walls
            radiused_box(
                outside_height = outside_height,
                outside_length = outside_length,
                outside_width = outside_width,
                outside_radius = or,
                wall_thickness = (minkowski ? 0.02 : wall_thickness),
                chamfer = (minkowski ? 0 : chamfer),
                manifold_overlap = true);
            
            // floor
            translate([0, -0, -((outside_height / 2) - (minkowski ? 0.01 : wall_thickness / 2))]) {
                hull() { 
                    radiused_box(
                        outside_height = (minkowski ? 0.01 : wall_thickness),
                        outside_length = outside_length,
                        outside_width = outside_width,
                        outside_radius = or,
                        wall_thickness = (minkowski ? 0.01 : wall_thickness),
                        chamfer = (minkowski ? 0 : chamfer),
                        manifold_overlap = true);
                }
            }
        }
    }
    
    module cutout() {
        translate([0, 0, (outside_height + wall_thickness / 2) / 2]) {
            rotate([0, 90, 0]) {
                resize([(oh * inch * cutout_ratio[1]), 0, 0]) {
                    mcylinder(d=oh * inch * cutout_ratio[0], h=ow * inch + wall_thickness * 3 + 1);
                }
            }
        }
    }

    
    module basic_box_with_cutout() {
        difference() {
            basic_box();
            cutout();
        }
    }

    difference() {
        union() {
            if(minkowski) {
                minkowski() {
                    basic_box_with_cutout();
                    sphere(wall_thickness / 2);
                }
            } else {
                basic_box_with_cutout();
            }    
        }
        union() {
            if(!is_undef(label) && label) {
                translate([0, ol * inch, default_wall_thickness / 2]) {

                    rotate([0, 180, 0]) 

                    linear_extrude(height = default_wall_thickness) {

                        spread = 5;
                        size = 4;

                        translate([0, -spread    , 0]) text(str(  "w/l/h/r"), size = size, halign = "center", valign = "center");
                        translate([0, -spread * 2, 0]) text(str(         ow), size = size, halign = "center", valign = "center");
                        translate([0, -spread * 3, 0]) text(str(         ol), size = size, halign = "center", valign = "center");
                        translate([0, -spread * 4, 0]) text(str(oh, "/", or), size = size, halign = "center", valign = "center");

                    }
                }
            }
        }
    }
}




if(!is_undef(debug) && debug) { 
    
    if(!is_undef(debug_slug) && debug_slug) {
        tool_slug();
    }  

    if(!is_undef(debug_model) && debug_model) {

        translate([0, 0,   0]) color("pink")       tool_holder(ow = 2, ol = 3, oh = 5, or = 10/*, minkowski = true*/);

    }
}


