// NOSTL

include <../libopenscad/mcube.scad>
include <../libopenscad/mtube.scad>
include <../libopenscad/mcylinder.scad>

inch = 25.4;

default_wall_thickness = inch / 8;
default_chamfer = default_wall_thickness / 4;

debug = true;

debug_cleat = true;
debug_rail = true;

debug_slug = true;
debug_model = true;
debug_cutaway = true;

default_height = 1;
default_width = 1;


colors = ["red", "white", "blue", "grey", "orange", "salmon", "pink", "green"];


default_debug_fn = 36;
default_finish_fn = 360;

//$fn = (!is_undef(debug) && !debug) ? 360 : default_debug_fn;



default_horn_arc = 75;
default_horn_arc_radius = inch / 5;
default_horn_diameter = inch * 0.225;
default_horn_standoff = 0.5;
default_plate_back_margin = 0.25;
default_plate_width_margin = 0.25;
default_plate_height_margin = 0.25;
default_horn_lean_in = 0;    
    

if(!is_undef(debug) && debug) { 
    
    
    
    if(!is_undef(debug_cleat) && debug_cleat) {

        intersection() {
            union() {
                if(!is_undef(debug_model) && debug_model) {
                    cleat_model();
                }
                if(!is_undef(debug_slug) && debug_slug) {
                    cleat_slug();
                }  

            } 
            union() {
                if(!is_undef(debug_cutaway) && debug_cutaway) {
                    color("red") {
                        mcube([100, 100, 100], align = [ -1, 0, 0]);
                    }
                }
            }
        }

    }
    
    
    if(!is_undef(debug_rail) && debug_rail) { 

        intersection() {
            union() {
                if(!is_undef(debug_model) && debug_model) {
                    rail_model(2,5);
                }
                if(!is_undef(debug_slug) && debug_slug) {
                    rail_slug();
                }  
            } 
            union() {
                if(!is_undef(debug_cutaway) && debug_cutaway) {
                    color("red") {
                        mcube([100, 100, 100], align = [ -1, 0, 0]);
                    }
                }
            }
        }
        

        
    }




    
}












module rail_slug(name = "rail") {
    echo(str(name, " slug"));
    
    // no slug makes sense
    
}

module rail_model(height = default_height, width = default_width, chamfer = default_chamfer, wall_thickness = default_wall_thickness, name = "rail", color = "Thistle") {
    
    height = height * inch; 
    width = width * inch;

    echo(str(name, " model"));

    translate([-((width / 2) - inch / 2), 0, 0]) {

        for(x = [0 : inch : width - 1]) {


            o_width = 1 * inch;
            i_width = o_width - wall_thickness * 2;
            
            translate([x, -wall_thickness * 3, 0]) {
                
                // back wall
                color(!is_undef(debug) && debug ? "lightblue" : color) {
                    mcube([o_width, wall_thickness, height], chamfer = chamfer, align = [0, 1, 1]);
                }

                
                // side walls
                color(!is_undef(debug) && debug ? "pink" : color) {
                    for(x = [-1, 1]) {
                        translate([x * o_width / 2, 0, 0]) {
                            mcube([wall_thickness, wall_thickness * 3, height], chamfer = chamfer, align = [-x, 1, 1]);
                        }
                    }
                }
                
                
                // front arms
                color(!is_undef(debug) && debug ? "lightgreen" : color) {
                    for(x = [-1, 1]) {
                        translate([x * o_width / 2, wall_thickness * 2, 0]) {
                            mcube([wall_thickness * 2, wall_thickness, height], chamfer = chamfer, align = [-x, 1, 1]);
                        }
                    }
                }
                
                

                // lid
                color(!is_undef(debug) && debug ? "salmon" : color) {
                    translate([0, 0, height]) {
                    mcube([o_width, wall_thickness * 3, wall_thickness], chamfer = chamfer, align = [0, 1, -1]);
                    }
                }
                
                
         
            }
        }

    
        for(x = [inch - (inch / 2): inch : width - inch]) {
            echo("x", x);
            translate([x, 0, 0]) {
                color(!is_undef(debug) && debug ? "thistle" : color) {
                    mcube([wall_thickness * 1.5, wall_thickness * 3, height], chamfer = chamfer, align = [0, -1, 1]);
                }
            }
        }
    }

}






module cleat_slug(height = default_height, wall_thickness = default_wall_thickness, name = "cleat", chamfer = default_chamfer) {
    echo(str(name, " slug"));

    height = height * inch; 

    extent = 3;

    difference() {    
        color("cyan") {
            mcube([inch * extent, 1, inch * extent], align = [0, 1, 1]);
        }

        color("red") {
            for(x = [-(extent - 1) / 2 : 1 : (extent - 1) / 2]) {
                for(z = [-(extent - 1) / 2 : 1 : (extent - 1) / 2]) {
                    translate([x * inch, 0, (z * inch) + (extent * inch / 2)]) {
                        rotate([90, 0, 0]) {
                            mcylinder(d = inch / 4, h = inch);
                        }
                    }
                }
            }
        }
    }    

}

module cleat_model(height = default_height, chamfer = default_chamfer, wall_thickness = default_wall_thickness, name = "cleat", color = "orange", 
    horn_arc = default_horn_arc,
    horn_arc_radius = default_horn_arc_radius,
    horn_diameter = default_horn_diameter,
    horn_standoff = default_horn_standoff,
    plate_back_margin = default_plate_back_margin,
    plate_width_margin = default_plate_width_margin,
    plate_height_margin = default_plate_height_margin,
    horn_lean_in = default_horn_lean_in) {
        
    height = height * inch; 

    echo(str(name, " model"));
//    color("orange") {
    union() {

 translate([0, -wall_thickness, 0]) {

        color(!is_undef(debug) && debug ? "lightblue" : color) {
        // base plate

                mcube([(inch * 1) - (wall_thickness * 2) - (plate_width_margin), 
                        wall_thickness - plate_back_margin, 
                        height - wall_thickness -plate_height_margin], 
                        chamfer = chamfer, 
                        align = [0, -1, 1]); 
            
            }
            color(!is_undef(debug) && debug ? "pink" : color) {
            // buttress

                intersection() {
                    translate([0, -wall_thickness, wall_thickness * 4]) {
                        mcube([(inch * 1) - (wall_thickness * 4) - (plate_width_margin), 
                                (wall_thickness * 2) - plate_back_margin, 
                                (wall_thickness * 4) - plate_height_margin], 
                                chamfer = wall_thickness - (plate_width_margin > plate_height_margin ? plate_height_margin : plate_width_margin), 
                                align = [0, 1, 0]); 
                    }

                    color("red") {
                    // clip back side of buttress so it doesn't poke through base plate after plate is "trimmed"

                        translate([0, -wall_thickness / 2, 0]) {
                            mcube([inch , inch, inch], align = [0, 1, 1]);
                        }
                    }
                }

            }
            
            
            color(!is_undef(debug) && debug ? "lightgreen" : color) {
            // standoff

                translate([0, -0.01, (height) / 2]) {
                    rotate([90, 0, 0]) {
                        mcylinder(d = horn_diameter, h = wall_thickness + horn_standoff + 0.02, align = [0, 0, -1]);
                    }  
                }

            }
            



        

        translate([0, wall_thickness, 0])
        // shift out to edge of buttress

        translate([0, horn_standoff, 0])
        // shift out to edge of standoff

        translate([0, 0, height / 2])
        // raise horn to the center of the plate

        rotate([horn_lean_in, 0, 0])
        // initial "lean-in" angle of horn

        translate([0, 0, horn_arc_radius])
        // raise horn so base (flat end) is at [0,0,0]

        rotate([0, 90, 0]) 
        // stand the horn up vertically

        union() {
        // the horn

            color(!is_undef(debug) && debug ? "salmon" : color)
            rotate_extrude(angle = horn_arc) {
            // body
                translate([horn_arc_radius, 0, 0]) {
                    circle(d = horn_diameter);
                }
            }


            color(!is_undef(debug) && debug ? "fuchsia" : color) {
                translate([horn_arc_radius, 0, 0]) {
                // round tip
                    sphere(d = horn_diameter);
                }
            rotate([0, 0, horn_arc]) {
                translate([horn_arc_radius, 0, 0]) {
                // round tip
                    sphere(d = horn_diameter);
                }
            }
            }
        }
        
        
    }

}
}

