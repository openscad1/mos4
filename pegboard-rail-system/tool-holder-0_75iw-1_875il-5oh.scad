include <tool-holder-common.scad>
include <rail.scad>




debug = false;
//debug_me = true;

tool_width = 0.75;
tool_length = 1.875;

    inside_length = tool_length + 1/8;
    inside_width = tool_width + 1/8;
    
    outside_height = 5;



if(!is_undef(debug_me) && debug_me) {
    intersection() {
        tool_holder();
        cube([100, 100, 100]);
    }
} else {
    
    $fn = 30;
    
    minkowski = true;
    
    wall_thickness = 1/8;
    
    outside_length = is_undef(inside_length) ? (outside_length) : (inside_length + (wall_thickness * 2));
    outside_width  = is_undef( inside_width) ? ( outside_width) : (inside_width  + (wall_thickness * 2));
    outside_height = is_undef(inside_height) ? (outside_height) : (inside_height + (wall_thickness * 2));

    difference() {
        union() {
            tool_holder(ol = outside_length, ow = outside_width, oh = outside_height, minkowski = is_undef(minkowski) ? false : minkowski);
            translate([0, inch * (outside_length + 0.25) + 1, 0]) {
                color("violet")
                rail_model(outside_height, 1);
            }
        }
        union() {
            translate([0, -inch / 2, 0.05]) {
                mcube([inch * (outside_width+1), inch * (outside_length+1), inch], color = "red", align = [0, 1, -1]);
            }
        }
    }
}


