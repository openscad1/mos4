include <../libopenscad/mbox.scad>

inch = 25.4;

$fn = 180;

scale = 0.5;
outside_length = inch * 5 * scale;
outside_width = inch * 3 * scale;
outside_height = 2.5;
chamfer = outside_height / 4;
wall_thickness = 10;


difference() {
//union() {
    union() {
        
        color("lightgreen")
        hull()
        radiused_box(inside_height = outside_height, 
            inside_length = outside_length - wall_thickness * 2,
            inside_width = outside_width, 
            inside_radius = outside_width / 32, 
            chamfer = chamfer,
            wall_thickness = wall_thickness, 
            align = [0, 0, -1]);
    }


    union() {
        color("green")
        rotate([0, 0, 90])
        translate([0, 0, -0.2])
        linear_extrude(4)
        text("yuck", halign = "center", valign = "center");
    }

}